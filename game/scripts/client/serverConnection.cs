//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

// Functions dealing with connecting to a server


//-----------------------------------------------------------------------------
// Server connection error
//-----------------------------------------------------------------------------

addMessageCallback( 'MsgConnectionError', handleConnectionErrorMessage );

function handleConnectionErrorMessage(%msgType, %msgString, %msgError)
{
   // On connect the server transmits a message to display if there
   // are any problems with the connection.  Most connection errors
   // are game version differences, so hopefully the server message
   // will tell us where to get the latest version of the game.
   $ServerConnectionErrorMessage = %msgError;
}


//----------------------------------------------------------------------------
// GameConnection client callbacks
//----------------------------------------------------------------------------

function GameConnection::initialControlSet(%this)
{
   echo ("*** Initial Control Object");

   // The first control object has been set by the server
   // and we are now ready to go.
   
   // first check if the editor is active
   if (!isToolBuild() || !Editor::checkActiveLoadDone())
   {
      if (Canvas.getContent() != PlayGui.getId())
      {
         Canvas.setContent(PlayGui);
      }
   }
}

function GameConnection::onControlObjectChange(%this)
{
   echo ("*** Control Object Changed");
   
   // Reset the current FOV to match the new object
   // and turn off any current zoom.
   resetCurrentFOV();
   turnOffZoom();
}

function GameConnection::setLagIcon(%this, %state)
{
   if (%this.getAddress() $= "local")
      return;
   LagIcon.setVisible(%state $= "true");
}

function GameConnection::onFlash(%this, %state)
{
   if (isObject(FlashFx))
   {
      if (%state)
      {
         FlashFx.enable();
      }
      else
      {
         FlashFx.disable();
      }
   }
}

// Called on the new connection object after connect() succeeds.
function GameConnection::onConnectionAccepted(%this)
{
   // Called on the new connection object after connect() succeeds.
   LagIcon.setVisible(false);
   
   // Startup the physics world on the client before any
   // datablocks and objects are ghosted over.
   physicsInitWorld( "client" );   
}

function GameConnection::onConnectionTimedOut(%this)
{
   // Called when an established connection times out
   disconnectedCleanup();
   MessageBoxOK( "Timed out from server", "The server connection has timed out.");
}

function GameConnection::onConnectionDropped(%this, %msg)
{
   // Established connection was dropped by the server
   disconnectedCleanup();
   MessageBoxOK( "Disconnected", "The server has dropped the connection: " @ %msg);
}

function GameConnection::onConnectionError(%this, %msg)
{
   // General connection error, usually raised by ghosted objects
   // initialization problems, such as missing files.  We'll display
   // the server's connection error message.
   disconnectedCleanup();
   MessageBoxOK( "Disconnected", $ServerConnectionErrorMessage @ " (" @ %msg @ ")" );
}


//----------------------------------------------------------------------------
// Connection Failed Events
//----------------------------------------------------------------------------

function GameConnection::onConnectRequestRejected( %this, %msg )
{
   switch$(%msg)
   {
      case "CR_INVALID_PROTOCOL_VERSION":
         %error = "Your version of Brickogame is not compatible with this server. (Invalid protocol error)";
      case "CR_INVALID_CONNECT_PACKET":
         %error = "Internal Error: badly formed network packet";
      case "CR_YOUAREBANNED":
         %error = "You are banned from playing on this server.";
      case "CR_SERVERFULL":
         %error = "This server is full.";
      case "CHR_PASSWORD":
         // XXX Should put up a password-entry dialog.
         if ($Client::Password $= "")
            MessageBoxOK( "Rejected from server", "This server requires a password.");
         else {
            $Client::Password = "";
            MessageBoxOK( "Rejected from server", "The password you've attempted to use is incorrect.");
         }
         return;
      case "CHR_PROTOCOL":
         %error = "Your version of Brickogame is not compatible with this server. (Invalid protocol error)";
      case "CHR_CLASSCRC":
         %error = "Your version of Brickogame is not compatible with this server. (Incompatible game classes)";
      case "CHR_INVALID_CHALLENGE_PACKET":
         %error = "Internal Error: Invalid server response packet";
      case "CHR_GAME":
         %error = "Your version of Brickogame is not compatible with this server. (Different game?)";
      default:
         %error = "Connection error.  Please try another server.  Error code: (" @ %msg @ ")";
   }
   disconnectedCleanup();
   MessageBoxOK( "Rejected from server", %error);
}

function GameConnection::onConnectRequestTimedOut(%this)
{
   disconnectedCleanup();
   MessageBoxOK( "Timed out from server", "Your connection to the server timed out." );
}


//-----------------------------------------------------------------------------
// rtb shit (msgCallbacks.cs in RTB)
//-----------------------------------------------------------------------------

addMessageCallback('MsgItemPickup', handleItemPickup);

function handleItemPickup(%msgType, %pickupString, %slot, %invName)
{
	%slot = %slot+1;

	%invName = detag(%invName);
	
	switch (%slot)
	{
		case (1):
			TxtInvSlot1.setValue(%invName);
		case (2):
			TxtInvSlot2.setValue(%invName);
		case (3):
			TxtInvSlot3.setValue(%invName);
		case (4):
			TxtInvSlot4.setValue(%invName);
		case (5):
			TxtInvSlot5.setValue(%invName);
		case (6):
			TxtInvSlot6.setValue(%invName);
		case (7):
			TxtInvSlot7.setValue(%invName);
		case (8):
			TxtInvSlot8.setValue(%invName);
		case (9):
			TxtInvSlot9.setValue(%invName);
		case (10):
			TxtInvSlot10.setValue(%invName);

	}
	
}
	
addMessageCallback('MsgEquipInv', handleEquipInv);

function handleEquipInv(%msgType, %msgString, %slot)
{
	%slot++;
	switch (%slot)
	{
		case (1):
			Slot1BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (2):
			Slot2BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (3):
			Slot3BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (4):
			Slot4BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (5):
			Slot5BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (6):
			Slot6BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (7):
			Slot7BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (8):
			Slot8BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (9):
			Slot9BG.setBitmap("art/gui/GUIBrickSideEquip.png");
		case (10):
			Slot10BG.setBitmap("art/gui/GUIBrickSideEquip.png");
	}
}

// serverCmd.cs

function serverCmdgetTeamList(%client)
{
	for(%t = 0; %t < $Pref::Server::TotalTeams; %t++)
	{
		%team = $Teams[%t];
		messageClient(%client,'MsgSendTeamList',"",%team,%t);

	}
}

function serverCmdgetPlayerTeamList(%client)
{
	for(%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		%cl = ClientGroup.getObject(%i);

		if(%cl.team !$= "")
		{
			%Team = %cl.team;
		}
		else
		{
			%Team = "<None>";
		}
		messageClient(%client,'MsgSendPlayerTeamList',"",%cl.Namebase,%Team,%cl);
	}
}

function serverCmdgetPlayerList(%client)
{
	for(%i = 0; %i < ClientGroup.getCount(); %i++)
	{
		%cl = ClientGroup.getObject(%i);
		if(%cl !$= %client)
		{
			%Friend = getFriends(%client, %cl);
			%Safe = getSafe(%client, %cl);
			messageClient(%client,'MsgSendPlayerList',"",%cl.Namebase,%Friend,%Safe,%cl);
		}
	}
}

function ServerCmdPlantBrick(%client)
{
	//echo(%client, " is planting brick");

	if(%client.player.tempBrick.isMoverGhost)
	{
		%brick = %client.player.tempBrick;

		%SrtPos = %brick.MStartPos;
		%EndPos = %brick.getTransform();
		%SrtX = getWord(%SrtPos,0);
		%SrtY = getWord(%SrtPos,1);
		%SrtZ = getWord(%SrtPos,2);

		%EndX = getWord(%EndPos,0);
		%EndY = getWord(%EndPos,1);
		%EndZ = getWord(%EndPos,2);

		if(%SrtX > %EndX)
		   %XDiff = %SrtX - %EndX;
		else
		   %XDiff = %EndX - %SrtX;

		if(%SrtY > %EndY)
		   %YDiff = %SrtY - %EndY;
		else
		   %YDiff = %EndY - %SrtY;

		if(%SrtZ > %EndZ)
		   %ZDiff = %SrtZ - %EndZ;
		else
		   %ZDiff = %EndZ - %SrtZ;
	}

	%time = $Sim::Time - %client.LastBrickTime; 
	if($Pref::Server::BlockScripts && %time < 0.1)
	{
		%client.LastBrickTime = $Sim::Time;
		return;
	}

	if($Pref::Server::BlockScripts && (!%client.isAdmin && !%client.isSuperAdmin))
	{
		if(%client.BrickStamina == 0)
		{
			%client.wantclearownbricks = 1;
			serverCmdClearOwnBricks(%client);
			%client.delete("You have been kicked for brick spamming.  Don't worry, your mess has been cleared for you.");
			messageAll('',"\c3Warning:  " @ %client.namebase @ " (" @ getRawIP(%client) @ ") has been auto-kicked for brick spamming and their bricks have been cleared.");
		}
		else if(%client.BrickStamina == 5)
		{
			messageAllExcept(%client,'',"\c3Warning:  " @ %client.namebase @ " (" @ getRawIP(%client) @ ") might be trying to spam the server.  They have continued trying to build even after having their building rights suspended.");
		}
		else if(%client.BrickStamina == 10)
		{
			messageAllExcept(%client,'',"\c3Warning:  " @ %client.namebase @ " is building too quickly and has had their building rights suspended.");
			messageClient(%client,'',"\c3Warning:  Your building rights have been suspended because you were building too quickly.");
			%client.HBR = 0;
		}
		else if(%client.BrickStamina == 20)
		{
			messageClient(%client,'',"\c3Warning:  You are running low on stamina.  You need to stop building so quickly.");
		}

		%client.BrickStamina--;
	}

	if(%client.HBR $= 0 && %client.player.tempBrick.isMoverGhost !$= 1)
	{
		messageClient(%client,'',"\c3You do not have Building Rights!");
		return;
	}
	if(%client.isImprisoned)
	{
		messageClient(%client,'',"\c5No building you imprisoned scum!");
		return;
	}
	if(%client.plantingPrice $= "")
		%client.plantingPrice = 1;		

        if($Pref::Server::TogglePlantingCosts $= 1 && %client.money < %client.plantingPrice && %client.player.tempBrick.isMoverGhost !$= 1 && (!%client.isAdmin || !%client.isSuperAdmin))
	{
		messageClient(%client,'',"\c4You cannot afford to plant this Brick!");
		return;
	}
	%player = %client.player;
	%tempBrick = %player.tempBrick;

	//## Delete Temp Brick

	if(%tempBrick.isBrickGhostMoving $= 1)
	{
		return;
	}
	if(%tempBrick)
	{
		if(%tempBrick.ismounted())
		{
			%solid = %tempBrick.getDataBlock();
			//create the new brick//
			%newBrick = new StaticShape()
			{
				datablock = %solid;
			};
			MissionCleanup.add(%newBrick);
			%player.brickcar.mountobject(%newbrick,%player.carmounts);
			return;
		}
		%solid = %tempBrick.getDataBlock();

		//create the new brick//
		%newBrick = new StaticShape()
		{
			datablock = %solid;
		};
		MissionCleanup.add(%newBrick);

		//intialize upper and lower attachement lists
		%newBrick.upSize = 0;
		%newBrick.downSize = 0;
		%newBrick.up[0] = -1;
		%newBrick.down[0] = -1;
		%eulerRot = %tempBrick.EulerRot;
		


		//check for stuff in the way
		%mask = $TypeMasks::StaticShapeObjectType;

		%tempBrickTrans = %tempBrick.getTransform();

		%tempBrickisRotated = %tempBrick.isRotated;
		//messageAll("",'%1',%tempBrickisRotated);
		//these will be where we start ray casting
		//the starting and ending positions are adjusted to match the rotation
		%tempBrickX = (getWord(%tempBrickTrans, 0));
		%tempBrickY = (getWord(%tempBrickTrans, 1));
		%tempBrickZ = (getWord(%tempBrickTrans, 2));

		%startX = %tempBrickX;
		%startY = %tempBrickY;
		%startZ = %tempBrickZ;

		%startZ += (%solid.z * 0.2) ;			//start at top of brick		

		%loopEndX = 1;
		%loopEndY = 1;
		%xStep = 1;
		%yStep = 1;


		%quatZ = getword(%tempBrickTrans, 5);
		%quatAng = getword(%tempBrickTrans, 6);

		//asumes bricks are never rotated off the verticle axis
		if(%quatZ == -1){
			%quatAng += $pi;
		}

		%angleTest = mFloor(%quatAng / $piOver2);


		if(%angleTest == 0){
			%startX += 0.25;
			%startY += 0.25;

			%loopEndX = %solid.x;
			%loopEndY = %solid.y;
			%xStep = 1;
			%yStep = 1;
		}
		else if(%angleTest == 1){
			%startX += 0.25;
			%startY -= 0.25;

			%loopEndX = %solid.y;
			%loopEndY = %solid.x * -1;
			%xStep = 1;
			%yStep = -1;


		}
		else if(%angleTest == 2){
			%startX -= 0.25;
			%startY -= 0.25;

			%loopEndX = %solid.x * -1;
			%loopEndY = %solid.y * -1;
			%xStep = -1;
			%yStep = -1;
		}
		else if(%angleTest == 3){
			%startX -= 0.25;
			%startY += 0.25;

			%loopEndX = %solid.y * -1;
			%loopEndY = %solid.x;
			%xStep = -1;
			%yStep = 1;
		}
		else{
			error("Error in ServerCmdPlantBrick(): %angleTest > 3 or < 0");
			return;
		}
		

		//===Find Tempbrick start and end box points===//
		/////////////////////////////////////////////////
		%tempBrickTrans = %tempBrick.getTransform();
			
		%tempBrickXpos = getWord(%tempBrickTrans, 0);
		%tempBrickYpos = getWord(%tempBrickTrans, 1);
		%tempBrickZpos = getWord(%tempBrickTrans, 2);

		//determine which way the check brick is facing

		%tempBrickAngle = getWord(%tempBrickTrans, 6);
		%vectorDir = getWord(%tempBrickTrans, 5);

		if(%vectorDir == -1)
			%tempBrickAngle += $pi;

		%tempBrickAngle /= $piOver2;
		%tempBrickAngle = mFloor(%tempBrickAngle + 0.1);

		if (%tempBrickAngle > 4)
			%tempBrickAngle -= 4;
		if (%tempBrickAngle <= 0)
			%tempBrickAngle += 4;
		
		//error("tempbrick angle = ", %tempbrickangle);
		
		//StartX/y is low, endX/Y is high
		%tempBrickStartX = %tempBrickXpos;
		%tempBrickStartY = %tempBrickYpos;
		%tempBrickEndX = %tempBrickXpos;
		%tempBrickEndY = %tempBrickYpos;

		%tempBrickData = %tempBrick.getDataBlock();

		if(%tempBrickAngle == 1)
		{
			%tempBrickEndX += %tempBrickData.y * 0.5;
			%tempBrickStartY -= %tempBrickData.x * 0.5;
		}
		else if(%tempBrickAngle == 2)
		{
			%tempBrickStartX -= %tempBrickData.x * 0.5;
			%tempBrickStartY -= %tempBrickData.y * 0.5;
		}
		else if(%tempBrickAngle == 3)
		{
			%tempBrickStartX -= %tempBrickData.y * 0.5;
			%tempBrickEndY += %tempBrickData.x * 0.5;
		}
		else if(%tempBrickAngle == 4)
		{
			%tempBrickEndX += %tempBrickData.x * 0.5;
			%tempBrickEndY += %tempBrickData.y * 0.5;
		}
		///////////////////////////////////////////////
		//===Done finding temp brick start and end===//

			
		//echo("projectile start ", %startX @ " " @ %startY @ " " @ %startZ);

		//find the middle 
		%xOffset = (%loopEndX * 0.5) / 2;		//convert from lego units, then divide by 2 to get the center
		%yOffset = (%loopEndY * 0.5) / 2;
		%zoffSet = (%solid.z * 0.2) / 2;	

		//echo("offset is ", %xoffset, " ", %yoffset, " ", %zoffset);

		//find the biggest dimension
		%radius = %solid.x * 0.5;

		if( (%solid.y * 0.5) > %radius)
		{
			//echo("y is bigger");
			%radius = %solid.y * 0.5;
		}

		if( (%solid.z * 0.2) > %radius)
		{
			//echo("z is bigger");
			%radius = %solid.z * 0.2;
		}
		//its a radius, so divide by 2;
		%radius /= 2;
		%radius += 0.5;
		
		//echo("Radius = ", %radius);

		%brickCenter = %tempBrickX + %xOffset @ " " @ %tempBrickY + %yOffset @ " " @ %tempBrickZ + %zOffset ;
		//echo("brick center is ", %brickCenter);
		%mask = $TypeMasks::StaticShapeObjectType;
		
		%attachment = 0;
		%hanging = 1;

		InitContainerRadiusSearch(%brickCenter, %radius, %mask);

		while ((%checkObj = containerSearchNext()) != 0){
			%xOverLap = 0;	
			%yOverLap = 0;
			%zOverLap = 0;	
			%zTouch = 0;	//is the brick is right up against another brick vertically? 
			
			%checkData = %checkObj.getDataBlock();

			if(%checkData.className $= "Brick" || %checkData.className $= "Baseplate"){

				%isTrusted = 0;

				for(%trusted = 0; %trusted < clientGroup.GetCount(); %trusted++)
				{
					%cl = clientGroup.getObject(%trusted);
					for(%safe = 0; %safe < %checkObj.Owner.FriendListNum + 1; %safe++)
					{
						if(%checkObj.Owner.FriendList[%safe] == %cl && %cl == %client)
						{
							%isTrusted = 1;
						}
					}

				}

				if(%checkObj.NoBuild == 1 && %isTrusted == 0 && %client.player.tempBrick.isMoverGhost !$= 1)
				{
					messageClient(%client,"","\c4Sorry, can't build here.");
					return;
				}

				if(%isTrusted == 0 && %checkObj.Owner.Secure == 1 && !%client.isAdmin && !%client.isSuperAdmin && %client.player.tempBrick.isMoverGhost !$= 1)
				{
					messageClient(%client,"",'\c4Sorry, cant build here. \c0%1\c4 has not made you his/her friend.',%checkObj.Owner.name);
					%newBrick.delete();
					return;
				}

				if(%checkObj.isBrickGhost != 1)
				{

				//%checkObj.setSkinName('Blue');
				%checkTrans = %checkObj.getTransform();

				%checkXpos = getWord(%checkTrans, 0);
				%checkYpos = getWord(%checkTrans, 1);
				%checkZpos = getWord(%checkTrans, 2);

				//---check for z over touching---
				%comp1 = round(%tempBrickZ + (%solid.z * 0.2));
				%comp2 = round(%checkZpos);
				if(%comp1 == %comp2){
					//placing under checkObj
					%attachedOnTop = true;
					%zTouch = 1;
				}
					
				%comp1 = round(%tempBrickZ);
				%comp2 = round(%checkZpos + (%checkData.Z * 0.2));
				if(%comp1 == %comp2){
					//placing ontop of checkObj
					%attachedOnTop = false;
					%zTouch = 1;
				}
				//---end ztouching check---

				%tempBottom = round(%tempBrickZ);
				%tempTop = round(%tempBrickZ + (%solid.z * 0.2));
				%checkBottom = round(%checkZpos);
				%checkObjscale = %checkObj.getScale();
				%checkTop = round(%checkZpos + ((%checkData.Z * getWord(%checkObjscale,2)) * 0.2));

				//---test for z overlaps---
				//echo("checktop = ", %checkTop);
				//echo("checkbottom = ", %checkbottom);
				//echo("temptop = ", %tempTop);
				//echo("tempbottom = ", %tempbottom);
				
				if(%tempBottom >= %checkBottom) {
					if(%checkTop > %tempBottom) {
						%zOverlap = 1;
						//%checkObj.setSkinName('red');
					}
				}
				if(%checkBottom >= %tempBottom){
					if(%tempTop > %checkBottom) {
						%zOverlap = 1;
						//%checkObj.setSkinName('red');
					}
				}
				//---end z overlap test---

				//determine which way the check brick is facing

				%checkAngle = getWord(%checkTrans, 6);
				%vectorDir = getWord(%checkTrans, 5);

				if(%vectorDir == -1)
					%checkAngle += $pi;

				%checkAngle /= $piOver2;
				%checkAngle = mFloor(%checkAngle + 0.1);

				if (%checkAngle > 4)
					%checkAngle -= 4;
				if (%checkAngle <= 0)
					%checkAngle += 4;


				//echo("%checkAngle == ", %checkangle);
				//echo("check x dim = ", %checkData.x);
				//echo("check y dim = ", %checkData.y);


				//StartX/y is low, endX/Y is high
				%checkStartX = %checkXpos;
				%checkStartY = %checkYpos;
				%checkEndX = %checkXpos;
				%checkEndY = %checkYpos;

				if(%checkAngle == 1)
				{
					%checkEndX += %checkData.y * 0.5;
					%checkStartY -= %checkData.x * 0.5;
				}
				else if(%checkAngle == 2)
				{
					%checkStartX -= %checkData.x * 0.5;
					%checkStartY -= %checkData.y * 0.5;
				}
				else if(%checkAngle == 3)
				{
					%checkStartX -= %checkData.y * 0.5;
					%checkEndY += %checkData.x * 0.5;
				}
				else if(%checkAngle == 4)
				{
					%checkEndX += %checkData.x * 0.5;
					%checkEndY += %checkData.y * 0.5;
				}
				
				
				//round everything
				%checkStartX = round(%checkStartX);
				%checkStartY = round(%checkStartY);
				%checkEndX = round(%checkEndX);
				%checkEndY = round(%checkEndY);
				%tempBrickStartX = round(%tempBrickStartX);
				%tempBrickStartY = round(%tempBrickStartY);
				%tempBrickEndX = round(%tempBrickEndX);
				%tempBrickEndY = round(%tempBrickEndY);
	
				//echo("check x ", %checkStartX, " ", %checkEndX);
				//echo("check y ", %checkStartY, " ", %checkEndY);
				//echo("tempBrick x ", %tempBrickStartX, " ", %tempBrickEndX);
				//echo("tempBrick y ", %tempBrickStartY, " ", %tempBrickEndY);
			

				//---test for x overlaps---
				if(%tempBrickStartX >= %checkStartX) {
					//echo(%checkEndX, " >= ", %tempBrickStartX, "? ", (%checkEndX >= %tempBrickStartX));
					if(%checkEndX > %tempBrickStartX) {
						%xOverlap = 1;
					}
				}
				if(%checkStartX >= %tempBrickStartX){
					if(%tempBrickEndX > %checkStartX) {
						%xOverlap = 1;
					}
				}
				//---end x overlap test---

				//---test for Y overlaps---
				if(%tempBrickStartY >= %checkStartY) {
					if(%checkEndY > %tempBrickStartY) {
						%yOverlap = 1;
					}
				}
				if(%checkStartY >= %tempBrickStartY){
					if(%tempBrickEndY > %checkStartY) {
						%yOverlap = 1;
					}
				}
				//---end Y overlap test---

				//echo("x overlap = ", %xoverlap);
				//echo("y overlap = ", %yoverlap);
				//echo("z overlap = ", %zoverlap);

				if(%xOverlap && %yOverlap && %zOverlap && !$Pref::Server::BuildThrough && %client.player.tempBrick.isMoverGhost !$= 1){	
					//the brick is inside another brick, no need to keep checking
					//tell the player
					messageClient(%client, 'MsgError', '\c4You can\'t put a brick inside another brick.');
					%newBrick.delete();
					return;
				}
				}

			}//end if brick/baseplate 

			if(%checkObj.dead != true) //dont attach to bricks tagged for destruction
			{
				if(%zTouch && %xOverlap && %yOverLap)
				{
					%attachment = 1;	//we've attached to at least 1 brick
					//%checkObj.setSkinName('White');
					//we've attached to a brick, so add it to the attachement list of the brick we're planting
					if(%attachedOnTop == true)
					{
						//echo("adding to up list");
						//brick we checked is higher than the brick we're planting
						%newBrick.up[%newBrick.upSize] = %checkObj;
						%newBrick.upSize++;
					}
					else
					{
						//echo("adding to down list");
						//brick we checked is lower than the brick we're planting
						%newBrick.down[%newBrick.downSize] = %checkObj;
						%newBrick.downSize++;

						//bricks attached on top of hanging brick should be counted as hanging also
						if(%checkObj.wasHung != true)
						{
							%hanging = 0;
						}
					}
				}
			}

		}//end while search result loop

		if(%attachment == 0)
		{
			if($Pref::Server::FloatingBricks == 0)
			{
				//we didnt find anything to attach to so error and bail
				messageClient(%client, 'MsgError', '\c4You can\'t put a brick in mid air.');
				%newBrick.delete();
				return;
			}
		}
		if($Pref::Server::BombRigging != 1 && %newBrick.Datablock $= StaticDynamite)
		{
			messageClient(%client, '', "\c4Bomb Rigging Disabled!");
			%newBrick.delete();
			return;
		}
		if($Pref::Server::BombRigging != 1 && %newBrick.Datablock $= StaticPlunger)
		{
			messageClient(%client, '', "\c4Bomb Rigging Disabled!");
			%newBrick.delete();
			return;
		}
		if(%newBrick.Datablock $= staticPlunger && $Pref::Server::BombRigging $= 1)
		{
			if(%client.plantedplunger $= 1)
			{
			messageClient(%client, '', "\c4You need to plant the Bomb Next!");
			%newBrick.delete();
			return;
			}
			if(%client.plantedbomb $= 1)
			{
			messageClient(%client, '', "\c4You already have a Bomb Placed!");
			%newBrick.delete();
			return;
			}
			if(%client.timerigged != 0)
			{
			messageClient(%client, '', "\c4You already have a Timed Bomb Placed!");
			%newBrick.delete();
			return;
			}
		}
		if(%newBrick.Datablock $= StaticDynamite && $Pref::Server::BombRigging $= 1)
		{	
			

			if(%client.plantedbomb $= 5)
			{
			messageClient(%client, '', "\c4You already have the Max Bombs Placed!");
			%newBrick.delete();
			return;
			}
			if(%client.timerigged != 0)
			{
			messageClient(%client, '', "\c4You already have a Timed Bomb Placed!");
			%newBrick.delete();
			return;
			}
		}
		if(%newBrick.Datablock $= StaticbrickFire && %client.fireBrickCount >= 2)
		{	
			if(%client.isAdmin != 1 || %client.isSuperAdmin != 1)
			{
			messageClient(%client, '', "\c4You can only have 2 Fire Bricks Placed!");		
			%newBrick.delete();
			return;
			}
		}
		if(%hanging == 1)
		{
			//we're hanging
			%newBrick.wasHung = true;
		}

		//success! 
		
		//put the brick where its supposed to be
		%newBrick.setTransform(%tempBrick.getTransform());
		if(%client.AutoColorMode == 1)
		{
			%newBrick.setSkinName($legoColor[%client.colorIndex]);		
			%newBrick.SkinName = %newBrick.getSkinName();
		}
		if(%client.AutoColorMode == 2)
		{
			%rnd = getRandom($TotalColors);
			%newBrick.setSkinName($legoColor[%rnd]);
			%newBrick.SkinName = %newBrick.getSkinName();
		}
		
		if(%newBrick.Datablock $= Staticbrick2x2FX)
		{
			if(%client.FXCount >= $Pref::Server::MaxFXBricks)
			{
				messageClient(%client,"",'\c4You have reached the Max Ammount of FX Bricks, which has been set at %1', $Pref::Server::MaxFXBricks);
				return;
			}
			else
			{
				%client.FXBrickSelected = %newBrick;
				commandtoclient(%client,'Push',brickFX);
			}
		}
		if(%eulerRot $= "")
		{
		%eulerRot = "0 0 0";
		}
		if(%tempBrick.isBrickGhostN $= 1)
		{
			%newBrick.setSkinName(%tempBrick.SkinNameS);
		}


		if(%tempBrick.isMoverGhost $= 1)
		{
			%brick = %newBrick;

			%SrtPos = %client.WrenchObject.getTransform();
			%EndPos = %brick.getTransform();
			%SrtX = getWord(%SrtPos,0);
			%SrtY = getWord(%SrtPos,1);
			%SrtZ = getWord(%SrtPos,2);

			%EndX = getWord(%EndPos,0);
			%EndY = getWord(%EndPos,1);
			%EndZ = getWord(%EndPos,2);

			%XDiff = %SrtX - %EndX;
			%YDiff = %SrtY - %EndY;
			%ZDiff = %EndZ - %SrtZ;

		
			%XDiff = %XDiff*2;
			%YDiff = %YDiff*2;
			%ZDiff = ((%ZDiff/3)/0.2);
		
			if(strstr(%ZDiff,".") !$= "-1")
			   %ZDiff = getSubStr(%ZDiff,0,strstr(%ZDiff,"."));

			%col = %client.WrenchObject;
			%col.isDoor = 1;
			%col.MoveXYZ = %XDiff SPC %YDiff SPC %ZDiff;
			%col.Steps = 100;
			%col.StepTime = 10;
			%col.RotateXYZ = "0 0 0";
			%col.ReturnDelay = 2000;
			%col.ReturnToggle = 0;
			%col.RotateXYZ = "0 0 0";
			%col.noCollision = 0;
			%col.isMoving = 0;
			%col.doorReturn = 1;
			%col.doorType = 3;
			commandtoclient(%client,'OpenMoverGui');

			%newBrick.delete();
			%tempBrick.delete();
			%client.player.tempBrick = "";
			return;
		}
		if(%client.plantingPrice $= "")
			%client.plantingPrice = 0;

		if($Pref::Server::TogglePlantingCosts $= 1 && (!%client.isAdmin || !%client.isSuperAdmin))
		{
			%client.money = %client.money - %client.plantingPrice;
			messageClient(%client,'MsgUpdateMoney','',%client.Money);
		}
		%newBrick.setScale(%tempBrick.getScale());
		%newBrick.EulerRot = %eulerRot;
		%newBrick.EulerRotation = %eulerRot;
		%newBrick.playAudio(0, brickPlantSound);
		%newBrick.Owner = %client;
		%newBrick.OwnerIP = getRawIP(%client);
		%newBrick.isRotated = %tempBrickisRotated;
		%client.LastBrickTime = $Sim::Time;
		%client.LastBrickPlaced = %newBrick;
		%client.Undo[0] = 0;
		%newBrick.ownername = %client.namebase;
		%client.Undo[1] = %newBrick;
		if(%newBrick.Datablock $= staticPlunger)
		{
			if(%client.plantedplunger != 1)
			{
				%client.plantedbomb = 0;
				%client.plantedplunger = 1;
				%newBrick.bombID = %client;
				%newBrick.NoBreak = 1;
				%newBrick.brickType = 1;
				%newBrick.NoDestroy = 1;
				%newBrick.riggerID = %client;	
				messageClient(%client,"","\c4You now need to plant some Dynamite.");

			}
			else
			{
				messageClient(%client,"","\c4You have already planted the Plunger. Plant your Bomb!");
			}
		
		}


		if(%newBrick.Datablock $= StaticDynamite)
		{
			if(%client.plantedplunger $= 1)
			{
				%client.plantedplunger = 1;
				%client.plantedbomb++;
				%newBrick.bombID = %client;
				%newBrick.NoBreak = 1;
				%newBrick.brickType = 2;
				%newBrick.NoDestroy = 1;
				%newBrick.riggerID = %client;
				if(%client.plantedbomb $= 1)
				{
		
					messageAll('name', '\c0%1\c5 has Rigged a Bomb!', %client.name);
					messageClient(%client,"","\c5You have Rigged a Bomb. Jump on the Detonator to Blow It, Or Plant More Bombs.");
				}
			}
			else
			{
				%client.timerigged = 1;
				%client.timedid++;
				%newBrick.cloaked = 1;
				%newBrick.bombID = %client.timedid;
				%newBrick.NoBreak = 1;
				%newBrick.NoDestroy = 1;
				%newBrick.riggerID = %client;
				messageClient(%client,"","\c5You Rigged a Timed Bomb. Its Set to Detonate in \c010 Seconds\c5!");
				%client.bombschedule = Schedule(10000,0,"ServerCmdBlowTimedBomb",%client,%newBrick,10000);
				messageAll('name', '\c0%1\c5 has Rigged a Timed Bomb!', %client.name);
			}
		}

		//update the attachment lists of the bricks we just attached to//
		for(%i = 0; %i < %newBrick.upSize; %i++)
		{
			%attachedBrick = %newBrick.up[%i];
			
			%attachedBrick.down[%attachedBrick.downSize] = %newBrick;
			%attachedBrick.downSize++;
		}
			
		for(%i = 0; %i < %newBrick.downSize; %i++)
		{
			//error("checking down");
			%attachedBrick = %newBrick.down[%i];

			%attachedBrick.up[%attachedBrick.upSize] = %newBrick;
			%attachedBrick.upSize++;
		}
		//done updating attachment lists//
	}
}

function ServerCmdScaleZup(%client)
{
	%player = %client.player;
	if(%player.tempBrick)
	{
	%tempBrick = %player.tempBrick;
	%scale = %tempBrick.getScale();
	%scaleX = getWord(%scale,0);
	%scaleY = getWord(%scale,1);
	%scaleZ = getWord(%scale,2);
	%finalscale = %scaleZ + 0.333333;
	if(%finalscale > 10) %finalscale = 10;
	%tempBrick.customscale = %scaleX SPC %scaleY SPC %finalscale;
	%tempBrick.setScale(%scaleX SPC %scaleY SPC %finalscale);
	}

}

function ServerCmdScaleZdown(%client)
{
	%player = %client.player;
	if(%player.tempBrick)
	{
	%tempBrick = %player.tempBrick;
	%scale = %tempBrick.getScale();
	%scaleX = getWord(%scale,0);
	%scaleY = getWord(%scale,1);
	%scaleZ = getWord(%scale,2);
	%finalscale = %scaleZ - 0.333333;
	if(%finalscale <= "0.333333")
	{
	%finalscale = "0.333333";
	}

	else if(%scaleZ <= "0.333333")
	{
	%finalscale = "0.333333";
	}
	%tempBrick.customscale = %scaleX SPC %scaleY SPC %finalscale;
	%tempBrick.setScale(%scaleX SPC %scaleY SPC %finalscale);
	}

}

function ServerCmdCancelBrick(%client)
{
	%player = %client.player;
	if(%player)
	{
		if(%player.tempBrick)
		{
			if(%player.tempBrick.isBrickGhostMoving $= 1)
			{
				%player.tempBrick.setSkinName(%player.tempBrick.oldskinname);
				%player.tempBrick.isBrickGhost = "";
				%player.tempBrick.isBrickGhostMoving = "";
				%player.tempBrick = "";
			}
			else
			{
				%player.tempBrick.delete();
				%player.tempBrick = "";
			}
		}
	}
}

function ServerCmdShiftBrick( %client, %x, %y, %z)
{
	if(%client.player.tempBrick.FXMode >= 1)
	   return;
	//z dimension is sent in PLATE HEIGHTS, not brick heights
	if(%client.ShiftSize $= 1)
	{
		serverCmdSMALLShiftBrick(%client, %x, %y, %z);
		return;
	}
	else if(%client.ShiftSize $= 2)
	{
		serverCmdBIGShiftBrick(%client, %x, %y, %z);
		return;
	}
	%player = %client.player;
	%tempBrick = %player.tempBrick;
	if(!isObject(%tempBrick))
	{
	return;
	}
	%tempbrick.setSkinName('construction');
	%carmounts = %player.carmounts;
	
	if(%tempBrick)
	{
	
        if(%tempBrick.ismounted())
	{   
            %player.carmounts++;
            %player.brickcar.mountobject(%tempbrick,%player.carmounts);
            if(%player.carmounts == 9)
            {
            %player.carmounts = "0";
            }
            if(%player.carmounts == -1)
            {
            %player.carmounts = "9";
            }
            if(%player.carmounts == 0)
            {
            %player.carmounts = "1";
            }
            return;
        }

		//%brickTrans = %tempBrick.getTransform();
		//%brickX = getWord(%brickTrans, 0);
		//%brickY = getWord(%brickTrans, 1);
		//%brickZ = getWord(%brickTrans, 2);
		//%brickRot = getWords(%brickTrans, 3, 6);

		%forwardVec = %player.getForwardVector();
		%forwardX = getWord(%forwardVec, 0);
		%forwardY = getWord(%forwardVec, 1);
		//echo("forwardvec = ", %forwardVec);

		if(%forwardX > 0){
			if(%forwardX > mAbs(%forwardY)){
				//we are facing basically x+
				//this is the default, dont do anything
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}
		else
		{
			if(mAbs(%forwardX) > mAbs(%forwardY)){
				//we are facing basically x-
				//this backwards from default, reverse everything
				%x *= -1;
				%y *= -1;
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}

		//Convert lego units to world units'
		%x *= 0.5;
		%y *= 0.5;
		%z *= 0.2;
		//if (%z)
		//{
			//%tempBrick.playthread(0, root);
			
			//%tempBrick.playthread(0, shiftUp);
			//%tempBrick.schedule(100, playthread, 0, root);
		//}
		//%tempBrick.setTransform(%brickX + %x @ " " @ %brickY + %y @ " " @ %brickZ + %z @ " " @ %brickRot);
		shift(%tempbrick, %x, %y, %z);

		ServerPlay3D(brickMoveSound, %tempBrick.getTransform());
	}
}

function ServerCmdSMALLShiftBrick( %client, %x, %y, %z)
{
	//z dimension is sent in PLATE HEIGHTS, not brick heights

	%player = %client.player;
	%tempBrick = %player.tempBrick;
	if(!isObject(%tempBrick))
	{
	return;
	}
	%tempbrick.setSkinName('construction');
	%carmounts = %player.carmounts;
	
	if(%tempBrick)
	{
	
        if(%tempBrick.ismounted())
	{   
            %player.carmounts++;
            %player.brickcar.mountobject(%tempbrick,%player.carmounts);
            if(%player.carmounts == 9)
            {
            %player.carmounts = "0";
            }
            if(%player.carmounts == -1)
            {
            %player.carmounts = "9";
            }
            if(%player.carmounts == 0)
            {
            %player.carmounts = "1";
            }
            return;
        }

		//%brickTrans = %tempBrick.getTransform();
		//%brickX = getWord(%brickTrans, 0);
		//%brickY = getWord(%brickTrans, 1);
		//%brickZ = getWord(%brickTrans, 2);
		//%brickRot = getWords(%brickTrans, 3, 6);

		%forwardVec = %player.getForwardVector();
		%forwardX = getWord(%forwardVec, 0);
		%forwardY = getWord(%forwardVec, 1);
		//echo("forwardvec = ", %forwardVec);

		if(%forwardX > 0){
			if(%forwardX > mAbs(%forwardY)){
				//we are facing basically x+
				//this is the default, dont do anything
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}
		else
		{
			if(mAbs(%forwardX) > mAbs(%forwardY)){
				//we are facing basically x-
				//this backwards from default, reverse everything
				%x *= -1;
				%y *= -1;
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}

		//Convert lego units to world units'
		%x *= 0.1;
		%y *= 0.1;
		%z *= 0.04;
		//if (%z)
		//{
			//%tempBrick.playthread(0, root);
			
			//%tempBrick.playthread(0, shiftUp);
			//%tempBrick.schedule(100, playthread, 0, root);
		//}
		//%tempBrick.setTransform(%brickX + %x @ " " @ %brickY + %y @ " " @ %brickZ + %z @ " " @ %brickRot);
		shift(%tempbrick, %x, %y, %z);

		ServerPlay3D(brickMoveSound, %tempBrick.getTransform());
	}
}

function ServerCmdBIGShiftBrick( %client, %x, %y, %z)
{
	//z dimension is sent in PLATE HEIGHTS, not brick heights

	%player = %client.player;
	%tempBrick = %player.tempBrick;
	if(!isObject(%tempBrick))
	{
	return;
	}

	%tempbrick.setSkinName('construction');
	
	%carmounts = %player.carmounts;


	if(%x == 1)
	{
		%x = %player.tempBrick.Datablock.x;
		if(%x == 1)
		{
			%x = 3;
		}
	}
	if(%y == 1)
	{
		%y = %player.tempBrick.Datablock.y;
		if(%y == 1)
		{
			%y = 3;
		}
	}
	if(%z == 1)
	{
		%z = %player.tempBrick.Datablock.z;
		if(%z == 1)
		{
			%z = 9;
		}
	}
	
	if(%x == -1)
	{
		%x = -%player.tempBrick.Datablock.x;
		if(%x == -1)
		{
			%x = -3;
		}
	}
	if(%y == -1)
	{
		%y = -%player.tempBrick.Datablock.y;
		if(%y == -1)
		{
			%y = -3;
		}
	}
	if(%z == -1)
	{
		%z = -%player.tempBrick.Datablock.z;
		if(%z == -1)
		{
			%z = -9;
		}
	}
	
	if(%tempBrick)
	{
	
        if(%tempBrick.ismounted())
	{   
            %player.carmounts++;
            %player.brickcar.mountobject(%tempbrick,%player.carmounts);
            if(%player.carmounts == 9)
            {
            %player.carmounts = "0";
            }
            if(%player.carmounts == -1)
            {
            %player.carmounts = "9";
            }
            if(%player.carmounts == 0)
            {
            %player.carmounts = "1";
            }
            return;
        }

		//%brickTrans = %tempBrick.getTransform();
		//%brickX = getWord(%brickTrans, 0);
		//%brickY = getWord(%brickTrans, 1);
		//%brickZ = getWord(%brickTrans, 2);
		//%brickRot = getWords(%brickTrans, 3, 6);

		%forwardVec = %player.getForwardVector();
		%forwardX = getWord(%forwardVec, 0);
		%forwardY = getWord(%forwardVec, 1);
		//echo("forwardvec = ", %forwardVec);

		if(%forwardX > 0){
			if(%forwardX > mAbs(%forwardY)){
				//we are facing basically x+
				//this is the default, dont do anything
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}
		else
		{
			if(mAbs(%forwardX) > mAbs(%forwardY)){
				//we are facing basically x-
				//this backwards from default, reverse everything
				%x *= -1;
				%y *= -1;
			}
			else{
				//we are closer to the y axis, but which direction?
				if(%forwardY > 0){
					//we are facing y +
					%newY = %x;
					%newX = -1 * %y; 
					%x = %newX;
					%y = %newY;
				}
				else{
					//we are facing y -
					%newY = -1 * %x;
					%newX = 1 * %y; 
					%x = %newX;
					%y = %newY;
				}
			}
		}

		//Convert lego units to world units'
		%x *= 0.5;
		%y *= 0.5;
		%z *= 0.2;
		//if (%z)
		//{
			//%tempBrick.playthread(0, root);
			
			//%tempBrick.playthread(0, shiftUp);
			//%tempBrick.schedule(100, playthread, 0, root);
		//}
		//%tempBrick.setTransform(%brickX + %x @ " " @ %brickY + %y @ " " @ %brickZ + %z @ " " @ %brickRot);
		shift(%tempbrick, %x, %y, %z);

		ServerPlay3D(brickMoveSound, %tempBrick.getTransform());
		%Trans = %tempBrick.getTransform();
		%X = getWord(%Trans, 0);
		%Y = getWord(%Trans, 1);
		%Z = getWord(%Trans, 2);
		%Pos = %X SPC %Y SPC %Z;
		messageClient(%client,'updateMBrick',"",%Pos,%obj.EulerRot);
	}
}


//moves something x,y,z absolute
function shift(%obj, %x, %y, %z)
{
	%trans = %obj.getTransform();

	%transX = getWord(%trans, 0);
	%transY = getWord(%trans, 1);
	%transZ = getWord(%trans, 2);
	%transQuat = getWords(%trans, 3, 6);

	%obj.setTransform(%transX + %x @ " " @ %transY + %y @ " " @ %transZ + %z @ " " @ %transQuat);
}

function ServerCmdRotateBrick( %client, %dir)
{
	//echo(%client, " is rotating brick dir = ", %dir);
	if(%client.player.tempBrick.FXMode >= 1 || %client.player.tempBrick.isMoverGhost $= 1)
	   return;
	%player = %client.player;
	%tempBrick = %player.tempBrick;
	if(!isObject(%tempBrick))
	{
	return;
	}
	if(%tempBrick.Datablock $= Staticbrick2x2FX)
	{
	return;
	}

	%tempbrick.setSkinName('construction');

	if(%tempBrick)
	{

		%tempBrick.currentRotSetting = %tempBrick.currentRotSetting - %dir;
		%Rot = %tempBrick.EulerRot;
		if(%Rot $= "")
		{
		%Rot = "0 0 0";
		}
		%RotX = getWord(%Rot, 0);
		%RotY = getWord(%Rot, 1);
		%RotZ = getWord(%Rot, 2);
		if(%tempBrick.currentRotSetting > 3)
		{
			%tempBrick.currentRotSetting = 0;
		}
		if(%tempBrick.currentRotSetting < 0)
		{
			%tempBrick.currentRotSetting = 3;
		}
		if(%tempBrick.currentRotSetting == 0)
		{
			%RotAngle = 0;
			%tempBrick.settransform(getwords(%tempBrick.gettransform(),0,2) SPC eulertoquat(%RotX SPC %RotY SPC %RotAngle));
			%tempBrick.EulerRot = %RotX SPC %RotY SPC %RotAngle;
		}
		if(%tempBrick.currentRotSetting == 1)
		{
			%RotAngle = 90;
			%tempBrick.settransform(getwords(%tempBrick.gettransform(),0,2) SPC eulertoquat(%RotX SPC %RotY SPC %RotAngle));
			%tempBrick.EulerRot = %RotX SPC %RotY SPC %RotAngle;
		}
		if(%tempBrick.currentRotSetting == 2)
		{
			%RotAngle = 180;
			%tempBrick.settransform(getwords(%tempBrick.gettransform(),0,2) SPC eulertoquat(%RotX SPC %RotY SPC %RotAngle));
			%tempBrick.EulerRot = %RotX SPC %RotY SPC %RotAngle;
		}
		if(%tempBrick.currentRotSetting == 3)
		{
			%RotAngle = 270;
			%tempBrick.settransform(getwords(%tempBrick.gettransform(),0,2) SPC eulertoquat(%RotX SPC %RotY SPC %RotAngle));
			%tempBrick.EulerRot = %RotX SPC %RotY SPC %RotAngle;
		}
		%Trans = %tempBrick.getTransform();
		%X = getWord(%Trans, 0);
		%Y = getWord(%Trans, 1);
		%Z = getWord(%Trans, 2);
		%Pos = %X SPC %Y SPC %Z;
		messageClient(%client,'updateMBrick',"",%Pos,%tempBrick.EulerRot);

	}



}

function serverCmdAddToInvent(%client, %position, %index)
{
	if($Pref::Server::Weapons == 0 && %index>=$StartWeapons && %index<= $Weapons)
	{
		return;
	}
	if(!%client.isEWandUser && !%client.isAdmin && !%client.isSuperAdmin && %index == $Special)
	{
		return;
	} 
	%player = %client.player;
	%data = %player.getDataBlock();
	
	%player.unMountImage($RightHandSlot);
	messageClient(%player.client, 'MsgHilightInv', '', -1);
	%player.currWeaponSlot = -1;

	%player.InvIndex[%position] = %index;
	%player.inventory[%position] = $Inv[%index,0];
	%inv = $Inv[%index,1];
	messageClient(%client, 'MsgItemPickup', '', %position, %inv);
	%client.CurrentInventoryPosition = %position;
	serverCmdUseInv(%client,%position);
}

function serverCmdCycleInventory(%client,%position,%shift)
{	
	%player = %client.player;
	if(%client.curInvPos == %position)
	{
		%client.curInvPos = %position;

		if(%position > 4)
		{
			if(%shift == 1)
			{
				servercmduseinv(%client,%position);
			}
			else
			{
				if(%position > 6)
				{
					servercmddropinv(%client,%position);
					%client.CurrentInventoryPosition = -1;
				}
				else if(%position > 4 && %position < 6)
				{
					if(%client.curSprayCan == 2)
					{
						%client.BlackletterIndex--;
						if(%client.BlackletterIndex < 0)
						{
							%client.BlackletterIndex = $TotalBlackLetters;
						}	
				commandtoclient(%client,'ShowBrickImage',$BlackLetterPreview[%client.BlackletterIndex]);
					}
					if(%client.curSprayCan == 0)
					{
						%client.colorIndex--;
						if(%client.colorIndex < 0)
						{
							%client.colorIndex = $TotalColors;
						}	
				commandtoclient(%client,'ShowBrickImage',$ColorPreview[%client.colorIndex]);
						}
					if(%client.curSprayCan == 1)
					{
						%client.letterIndex--;
						if(%client.letterIndex < 0)
						{
							%client.letterIndex = $TotalLetters;
						}	
				commandtoclient(%client,'ShowBrickImage',$LetterPreview[%client.letterIndex]);
					}

				}
			
			}
			
		}
		if(%position == 3)
		{
			if((%player.InvIndex[%position]+%shift)>$Misc)
			{
				%player.InvIndex[%position] = $StartMisc-1;
			}	
			if((%player.InvIndex[%position]+%shift)<$StartMisc)
			{
				%player.InvIndex[%position] = $Misc+1;
			}	
			serverCmdAddtoInvent(%client,%position,%player.InvIndex[%position]+%shift);
		}
		if(%position == 4)
		{
			if($Pref::Server::Weapons == 0)
			{
				if((%player.InvIndex[%position]+%shift)>$Tools)
				{
					%player.InvIndex[%position] = $StartTools-1;
				}	
				if((%player.InvIndex[%position]+%shift)<$StartTools)
				{
					%player.InvIndex[%position] = $Tools+1;
				}
			}	
			else
			{
				if((%player.InvIndex[%position]+%shift)>$Weapons)
				{
					%player.InvIndex[%position] = $StartTools-1;
				}	
				if((%player.InvIndex[%position]+%shift)<$StartTools)
				{
					%player.InvIndex[%position] = $Weapons+1;
				}

			}
			serverCmdAddtoInvent(%client,%position,%player.InvIndex[%position]+%shift);
		}

		if(%position == 2)
		{
			if((%player.InvIndex[%position]+%shift)>$Slopes)
			{
				%player.InvIndex[%position] = $StartSlopes-1;
			}	
			if((%player.InvIndex[%position]+%shift)<$StartSlopes)
			{
				%player.InvIndex[%position] = $Slopes+1;
			}	
			serverCmdAddtoInvent(%client,%position,%player.InvIndex[%position]+%shift);
		}

		if(%position == 1)
		{
			if((%player.InvIndex[%position]+%shift)>$Plates)
			{
				%player.InvIndex[%position] = $StartPlates-1;
			}	
			if((%player.InvIndex[%position]+%shift)<$StartPlates)
			{
				%player.InvIndex[%position] = $Plates+1;
			}	
			serverCmdAddtoInvent(%client,%position,%player.InvIndex[%position]+%shift);
		}

		if(%position == 0)
		{
			if((%player.InvIndex[%position]+%shift)>$Bricks)
			{
				%player.InvIndex[%position] = $StartBricks-1;
			}	
			if((%player.InvIndex[%position]+%shift)<$StartBricks)
			{
				%player.InvIndex[%position] = $Bricks+1;
			}	
			serverCmdAddtoInvent(%client,%position,%player.InvIndex[%position]+%shift);
		}
		
	}	
	else
	{
		%client.curInvPos = %position;
		servercmduseinv(%client,%position);
		if(%position>7)
		{
			%client.CurrentInventoryPosition = -1;
		}		
	}
}

function serverCmdUseInventory(%client,%position)
{
	if($Pref::Server::UseInventory == 1)
	{
		%shift = 1;
		if(%position < 8)
		{
			%client.CurrentInventoryPosition = %position;
		}
		servercmdCycleInventory(%client,%position,%shift);
	}
	else
	{
		servercmduseinv(%client,%position);
		%client.CurrentInventoryPosition = -1;
	}
	
}

function ServerCmduseInv( %client, %position)
{
		%player = %client.player;

		if(%player.inventory[%position])
			%item = %player.inventory[%position].getId();
		else
			%item = 0;

		if(%item)
		{
			%item.onUse(%player, %position); 
		}
}

//-----------------------------------------------------------------------------
// Disconnect
//-----------------------------------------------------------------------------

function disconnect()
{
   // We need to stop the client side simulation
   // else physics resources will not cleanup properly.
   physicsStopSimulation( "client" );

   // Delete the connection if it's still there.
   if (isObject(ServerConnection))
      ServerConnection.delete();
      
   disconnectedCleanup();

   // Call destroyServer in case we're hosting
   destroyServer();
}

function disconnectedCleanup()
{
   // End mission, if it's running.
   
   if( $Client::missionRunning )
      clientEndMission();
      
   // Disable mission lighting if it's going, this is here
   // in case we're disconnected while the mission is loading.
   
   $lightingMission = false;
   $sceneLighting::terminateLighting = true;
   
   // Clear misc script stuff
   HudMessageVector.clear();
   
   //
   LagIcon.setVisible(false);
   PlayerListGui.clear();
   
   // Clear all print messages
   clientCmdclearBottomPrint();
   clientCmdClearCenterPrint();

   // Back to the launch screen
   if (isObject( MainMenuGui ))
      Canvas.setContent( MainMenuGui );

   // Before we destroy the client physics world
   // make sure all ServerConnection objects are deleted.
   if(isObject(ServerConnection))
   {
      ServerConnection.deleteAllObjects();
   }
   
   // We can now delete the client physics simulation.
   physicsDestroyWorld( "client" );                 
}
