//Callbacks
function TCPObject::onConnected(%this)
{
        //You connected to the server successfully!
		echo(%this @ " connected");
}

function TCPObject::onLine(%this,%line)
{
        //%line contains data that was sent to us.
		echo(%this @ " online");
}

function TCPObject::onConnectFailed(%this)
{
        //You couldn't connect to the server.
		error(%this @ " failed to connect");
}

function TCPObject::onDisconnect(%this)
{
        //When you are disconnected via server or by disconnecting from the client.
}

function TCPObject::onDNSFailed(%this)
{
        //DNS Failure
}

function TCPObject::onDNSResolved(%this)
{
        //DNS was resolved.
}