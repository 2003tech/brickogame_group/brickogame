function startMissionGui::onSleep(%this)
{
	//save the server prefs from the screen
	//will this work? we might have started the server by the time this gets called.
	//UPDATE: nope, it doesnt
		
	
}
function startMissionGui::onWake(%this)
{
	SM_missionList.clear();
	%i = 0;
	for(%file = findFirstFile($Server::MissionFileSpec); %file !$= ""; %file = findNextFile($Server::MissionFileSpec))
	{
	   %LevelInfoObject = getLevelInfo(%file);

	   if (%LevelInfoObject != 0)
	   {
		  if(%LevelInfoObject.levelName !$= "")
			 %levelName = %LevelInfoObject.levelName;
		  else if(%LevelInfoObject.name !$= "")
			 %levelName = %LevelInfoObject.name;

		  if (%LevelInfoObject.desc0 !$= "")
			 %levelDesc = %LevelInfoObject.desc0;
			 
		  %LevelInfoObject.delete();
	   }
		SM_missionList.addRow(%i++, %levelName @ "\t" @ %file );
		SM_missionList.sort(0);
		SM_missionList.setSelectedRow(0);
		SM_missionList.scrollVisible(0);
	}

	//load the saved server prefs
	TxtServerName.setValue($Pref::Server::BaseServerName);
	TxtServerInfo.setValue($Pref::Server::Info);
	TxtServerMOTD.setValue($Pref::Server::MOTD);
	TxtServerPassword.setValue($Pref::Server::Password);
	TxtServerAdminPassword.setValue($Pref::Server::AdminPassword);
	AutoSecure.setValue($Pref::Server::AutoSecure);
	AdminEditor.setValue($Pref::Server::AdminEditor);
	StartMoney.setValue($Pref::Server::StartMoney);
	CheckItemsCostMoney.setValue($Pref::Server::ItemsCostMoney);
	OptElevators.setValue($Pref::Server::EnabledElevator);
	OptSitting.setValue($Pref::Server::EnabledSit);

	SliderNumPlayers.setValue($Pref::Server::MaxPlayers);
	MaxMovers.setValue($Pref::Server::MaxDoors);

}

function SM_missionList::onSelect(%this, %id, %text)
{
	%mission = getField(SM_missionList.getRowTextById(%id), 1);
	%mission = getSubStr(%mission, 0, Strstr(%mission, "."));
	SM_Img.setBitmap("levels/"@%mission@"_preview.png");
	echo("levels/"@%mission@"_preview.png");
}

function SM_StartMission()
{
	//save the pref values
	$Pref::Server::BaseServerName = TxtServerName.getValue();
	$Pref::Server::Name = $Pref::Server::BaseServerName;
	$Pref::Server::Info = TxtServerInfo.getValue();
	$Pref::Server::Password = TxtServerPassword.getValue();
	$Pref::Server::AdminPassword = TxtServerAdminPassword.getValue();
	$Pref::Server::MaxPlayers = SliderNumPlayers.getValue();
	$Pref::Server::AutoSecure = AutoSecure.getValue();
	$Pref::Server::AdminEditor = AdminEditor.getValue();
	$Pref::Server::MaxDoors = MaxMovers.getValue();
	$Pref::Server::StartMoney = StartMoney.getValue();
	$Pref::Server::ItemsCostMoney = CheckItemsCostMoney.getValue();
	$Pref::Server::MOTD = TxtServerMOTD.getValue();
	$Pref::Server::EnabledElevator = OptElevators.getValue();
	$Pref::Server::EnabledSit = OptSitting.getValue();
	//set your join password so you can join your own server
	$Client::Password = TxtServerPassword.getValue();


   %id = SM_missionList.getSelectedId();
   %mission = getField(SM_missionList.getRowTextById(%id), 1);

   if ($pref::HostMultiPlayer)
      %serverType = "MultiPlayer";
   else
      %serverType = "SinglePlayer";

   createAndConnectToLocalServer( %serverType, %mission );
}


//----------------------------------------
function getMissionDisplayName( %missionFile ) 
{
   %file = new FileObject();
   
   %MissionInfoObject = "";
   
   if ( %file.openForRead( %missionFile ) ) {
		%inInfoBlock = false;
		
		while ( !%file.isEOF() ) {
			%line = %file.readLine();
			%line = trim( %line );
			
			if( %line $= "new ScriptObject(MissionInfo) {" )
				%inInfoBlock = true;
			else if( %inInfoBlock && %line $= "};" ) {
				%inInfoBlock = false;
				%MissionInfoObject = %MissionInfoObject @ %line; 
				break;
			}
			
			if( %inInfoBlock )
			   %MissionInfoObject = %MissionInfoObject @ %line @ " "; 	
		}
		
		%file.close();
	}
	%MissionInfoObject = "%MissionInfoObject = " @ %MissionInfoObject;
	eval( %MissionInfoObject );
	
   %file.delete();

   if( %MissionInfoObject.name !$= "" )
      return %MissionInfoObject.name;
   else
      return fileBase(%missionFile); 
}
