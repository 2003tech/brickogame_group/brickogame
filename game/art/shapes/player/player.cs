//-----------------------------------------------------------------------------
// Torque Game Engine 
// Copyright (C) GarageGames.com, Inc.
//-----------------------------------------------------------------------------


datablock TSShapeConstructor(MiniFigDts)
{
   baseShape = "art/shapes/player/minifig.dts";
   sequence0 = "art/shapes/player/minifig_root.dsq root";
   sequence1 = "art/shapes/player/minifig_run.dsq run";
   sequence2 = "art/shapes/player/minifig_back.dsq back";
   sequence3 = "art/shapes/player/minifig_side.dsq side";
   sequence4 = "art/shapes/player/minifig_look.dsq look";
   sequence5 = "art/shapes/player/minifig_crouch.dsq crouch";
   sequence6 = "art/shapes/player/minifig_fall.dsq fall";
   sequence7 = "art/shapes/player/minifig_root.dsq land";
   sequence8 = "art/shapes/player/minifig_standjump.dsq jump";
   sequence9  = "art/shapes/player/minifig_crouchRun.dsq crouchRun";
   sequence10 = "art/shapes/player/minifig_crouchBack.dsq crouchBack";
   sequence11 = "art/shapes/player/minifig_crouchSide.dsq crouchSide";
   sequence12 = "art/shapes/player/minifig_root.dsq walk";
   sequence13 = "art/shapes/player/minifig_root.dsq death5";
   sequence14 = "art/shapes/player/minifig_root.dsq death6";
   sequence15 = "art/shapes/player/minifig_root.dsq death7";
   sequence16 = "art/shapes/player/minifig_root.dsq death8";
   sequence17 = "art/shapes/player/minifig_root.dsq death9";
   sequence18 = "art/shapes/player/minifig_spearready.dsq spearready";  
   sequence19 = "art/shapes/player/minifig_spearThrow.dsq spearThrow";
   sequence20 = "art/shapes/player/minifig_talk.dsq talk";
   sequence21 = "art/shapes/player/minifig_headup.dsq headUp";

   sequence22 = "art/shapes/player/minifig_talk.dsq talk";
   sequence23 = "art/shapes/player/minifig_headside.dsq headside";
   sequence24 = "art/shapes/player/minifig_standjump.dsq standjump";
   sequence25 = "art/shapes/player/minifig_headup.dsq headUp";
   sequence26 = "art/shapes/player/minifig_armAttack.dsq armAttack";
   sequence27 = "art/shapes/player/minifig_armReadyLeft.dsq armReadyLeft";
   sequence28 = "art/shapes/player/minifig_armReadyRight.dsq armReadyRight";
   sequence29 = "art/shapes/player/minifig_armReadyBoth.dsq armReadyBoth";
};    

